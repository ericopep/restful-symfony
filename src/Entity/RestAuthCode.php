<?php
namespace App\Entity;

use FOS\OAuthServerBundle\Entity\AuthCode as BaseAuthCode;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RestAuthCodeRepository")
 */
class RestAuthCode extends BaseAuthCode
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"full"})
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="RestClient")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"full"})
     */
    protected $client;

    /**
     * @ORM\ManyToOne(targetEntity="RestUser")
     * @Groups({"full"})
     */
    protected $user;
}
