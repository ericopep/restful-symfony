<?php
namespace App\Entity;

use FOS\OAuthServerBundle\Entity\AccessToken as BaseAccessToken;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RestAccessTokenRepository")
 */
class RestAccessToken extends BaseAccessToken
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"full"})
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="RestClient")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"full"})
     */
    protected $client;

    /**
     * @ORM\ManyToOne(targetEntity="RestUser")
     * @Groups({"full"})
     */
    protected $user;
}