<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CustomerRepository")
 */
class Customer
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"full"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"full", "basic"})
     * @Assert\NotNull()
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=11)
     * @Groups({"full", "basic"})
     * @Assert\NotNull()
     * @Assert\Length(
     *      min = 11,
     *      max = 11
     * )
     */
    private $cpf;

    /**
     * @ORM\Column(type="date")
     * @Groups({"full", "basic"})
     * @Assert\NotNull()
     * @Assert\Date
     */
    private $dataNascimento;

    //GETS
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getCpf(): ?string
    {
        return $this->cpf;
    }

    public function getDataNascimento(): ?\DateTime
    {
        return $this->dataNascimento;
    }

    //SETS
    public function setId($id): void
    {
        $this->id = $id;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    public function setCpf($cpf): void
    {
        $this->cpf = $cpf;
    }

    public function setDataNascimento($dataNascimento): void
    {
        $this->dataNascimento = new \DateTime($dataNascimento);
    }
}