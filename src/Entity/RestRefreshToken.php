<?php
namespace App\Entity;

use FOS\OAuthServerBundle\Entity\RefreshToken as BaseRefreshToken;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RestRefreshTokenRepository")
 */
class RestRefreshToken extends BaseRefreshToken
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"full"})
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="RestClient")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"full"})
     */
    protected $client;

    /**
     * @ORM\ManyToOne(targetEntity="RestUser")
     * @Groups({"full"})
     */
    protected $user;
}
