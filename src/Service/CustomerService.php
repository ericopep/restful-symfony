<?php
namespace App\Service;

use App\Repository\CustomerRepository;
use App\Entity\Customer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CustomerService
{
    private $customerRepository;
    private $validatorInterface;
    private $entityManager;

    public function __construct(
        CustomerRepository $customerRepository,
        ValidatorInterface $validatorInterface,
        EntityManagerInterface $entityManager
    ) {
        $this->customerRepository = $customerRepository;
        $this->validatorInterface = $validatorInterface;
        $this->entityManager = $entityManager;
    }

    public function getAllCustomers(): array
    {
        return $this->customerRepository->findAll();
    }
    
    public function getCustomers($request): array
    {
        return $this->customerRepository->filtroCustomers($request);
    }

    public function getCustomer($customer): Customer
    {
        return $this->customerRepository->findOneById($customer);
    }

    public function insertCustomer($request)
    {
        $customer = new Customer;
        $customer->setName($request->query->get('name'));
        $customer->setCpf($request->query->get('cpf'));
        $customer->setDataNascimento($request->query->get('dataNascimento'));

        $errors = $this->validatorInterface->validate($customer);

        if (count($errors) > 0) {
            return (string) $errors;
        } else {
            $this->entityManager->persist($customer);
            $this->entityManager->flush();
            return $customer;
        }
    }

    public function updateCustomer($request, $customer)
    {
        $customer = $this->customerRepository->findOneById($customer);;
        $customer->setName($request->query->get('name'));
        $customer->setCpf($request->query->get('cpf'));
        $customer->setDataNascimento($request->query->get('dataNascimento'));

        $errors = $this->validatorInterface->validate($customer);

        if (count($errors) > 0) {
            return (string) $errors;
        } else {
            $this->entityManager->flush();
            return $customer;
        }
    }

    public function deleteCustomer($customer): bool
    {
        $customer = $this->customerRepository->findOneById($customer);;
        $this->entityManager->remove($customer);
        $this->entityManager->flush();
        return true;
    }
}