<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181203170435 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE rest_access_token (id INT AUTO_INCREMENT NOT NULL, client_id INT NOT NULL, user_id INT DEFAULT NULL, token VARCHAR(255) NOT NULL, expires_at INT DEFAULT NULL, scope VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_3D108A505F37A13B (token), INDEX IDX_3D108A5019EB6921 (client_id), INDEX IDX_3D108A50A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rest_client (id INT AUTO_INCREMENT NOT NULL, random_id VARCHAR(255) NOT NULL, redirect_uris LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', secret VARCHAR(255) NOT NULL, allowed_grant_types LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rest_user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', UNIQUE INDEX UNIQ_FBF0850C92FC23A8 (username_canonical), UNIQUE INDEX UNIQ_FBF0850CA0D96FBF (email_canonical), UNIQUE INDEX UNIQ_FBF0850CC05FB297 (confirmation_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rest_refresh_token (id INT AUTO_INCREMENT NOT NULL, client_id INT NOT NULL, user_id INT DEFAULT NULL, token VARCHAR(255) NOT NULL, expires_at INT DEFAULT NULL, scope VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_EFC62B5C5F37A13B (token), INDEX IDX_EFC62B5C19EB6921 (client_id), INDEX IDX_EFC62B5CA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE customer (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, cpf VARCHAR(11) NOT NULL, data_nascimento DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rest_auth_code (id INT AUTO_INCREMENT NOT NULL, client_id INT NOT NULL, user_id INT DEFAULT NULL, token VARCHAR(255) NOT NULL, redirect_uri LONGTEXT NOT NULL, expires_at INT DEFAULT NULL, scope VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_236C0C505F37A13B (token), INDEX IDX_236C0C5019EB6921 (client_id), INDEX IDX_236C0C50A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE rest_access_token ADD CONSTRAINT FK_3D108A5019EB6921 FOREIGN KEY (client_id) REFERENCES rest_client (id)');
        $this->addSql('ALTER TABLE rest_access_token ADD CONSTRAINT FK_3D108A50A76ED395 FOREIGN KEY (user_id) REFERENCES rest_user (id)');
        $this->addSql('ALTER TABLE rest_refresh_token ADD CONSTRAINT FK_EFC62B5C19EB6921 FOREIGN KEY (client_id) REFERENCES rest_client (id)');
        $this->addSql('ALTER TABLE rest_refresh_token ADD CONSTRAINT FK_EFC62B5CA76ED395 FOREIGN KEY (user_id) REFERENCES rest_user (id)');
        $this->addSql('ALTER TABLE rest_auth_code ADD CONSTRAINT FK_236C0C5019EB6921 FOREIGN KEY (client_id) REFERENCES rest_client (id)');
        $this->addSql('ALTER TABLE rest_auth_code ADD CONSTRAINT FK_236C0C50A76ED395 FOREIGN KEY (user_id) REFERENCES rest_user (id)');

        $this->addSql('INSERT INTO rest_client VALUES (NULL, "3bcbxd9e24g0gk4swg0kwgcwg4o8k8g4g888kwc44gcc0gwwk4", "a:0:{}", "4ok2x70rlfokc8g0wws8c8kwcokw80k44sg48goc0ok4w0so0k", "a:1:{i:0;s:8:\"password\";}")');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE rest_access_token DROP FOREIGN KEY FK_3D108A5019EB6921');
        $this->addSql('ALTER TABLE rest_refresh_token DROP FOREIGN KEY FK_EFC62B5C19EB6921');
        $this->addSql('ALTER TABLE rest_auth_code DROP FOREIGN KEY FK_236C0C5019EB6921');
        $this->addSql('ALTER TABLE rest_access_token DROP FOREIGN KEY FK_3D108A50A76ED395');
        $this->addSql('ALTER TABLE rest_refresh_token DROP FOREIGN KEY FK_EFC62B5CA76ED395');
        $this->addSql('ALTER TABLE rest_auth_code DROP FOREIGN KEY FK_236C0C50A76ED395');
        $this->addSql('DROP TABLE rest_access_token');
        $this->addSql('DROP TABLE rest_client');
        $this->addSql('DROP TABLE rest_user');
        $this->addSql('DROP TABLE rest_refresh_token');
        $this->addSql('DROP TABLE customer');
        $this->addSql('DROP TABLE rest_auth_code');
    }
}
