<?php
namespace App\Repository;

use App\Entity\Customer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Customer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Customer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Customer[]    findAll()
 * @method Customer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Customer::class);
    }

    public function filtroCustomers($request): array
    {
    	//Pagina padrão
    	$offset = 0;
    	//Limite Padrão
        $limit = 10;

        $buscaCustomers = $request->query->get('busca');
        $paginaCustomers = $request->query->get('pagina');
        $limiteCustomers = $request->query->get('limite');
        
        if($limiteCustomers)
        	$limit = $limiteCustomers;
        if($paginaCustomers)
        	$offset = ($paginaCustomers - 1)*$limit;
        
        $qb = $this->createQueryBuilder('customer')
            ->andWhere('customer.name LIKE :busca')
            ->setFirstResult( $offset )
    		->setMaxResults( $limit )
            ->setParameter('busca', '%'.$buscaCustomers.'%')
            ->getQuery();

        return $qb->execute();
    }
}
