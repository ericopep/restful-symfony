<?php
namespace App\Repository;

use App\Entity\RestClient;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method RestClient|null find($id, $lockMode = null, $lockVersion = null)
 * @method RestClient|null findOneBy(array $criteria, array $orderBy = null)
 * @method RestClient[]    findAll()
 * @method RestClient[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RestClientRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, RestClient::class);
    }
}
