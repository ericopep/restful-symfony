<?php
namespace App\Repository;

use App\Entity\RestAuthCode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method RestAuthCode|null find($id, $lockMode = null, $lockVersion = null)
 * @method RestAuthCode|null findOneBy(array $criteria, array $orderBy = null)
 * @method RestAuthCode[]    findAll()
 * @method RestAuthCode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RestAuthCodeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, RestAuthCode::class);
    }
}
