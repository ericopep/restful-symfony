<?php
namespace App\Repository;

use App\Entity\RestUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method RestUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method RestUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method RestUser[]    findAll()
 * @method RestUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RestUserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, RestUser::class);
    }
}
