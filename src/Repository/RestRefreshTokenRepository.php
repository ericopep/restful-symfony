<?php
namespace App\Repository;

use App\Entity\RestRefreshToken;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method RestRefreshToken|null find($id, $lockMode = null, $lockVersion = null)
 * @method RestRefreshToken|null findOneBy(array $criteria, array $orderBy = null)
 * @method RestRefreshToken[]    findAll()
 * @method RestRefreshToken[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RestRefreshTokenRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, RestRefreshToken::class);
    }
}
