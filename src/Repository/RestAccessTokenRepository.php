<?php
namespace App\Repository;

use App\Entity\RestAccessToken;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method RestAccessToken|null find($id, $lockMode = null, $lockVersion = null)
 * @method RestAccessToken|null findOneBy(array $criteria, array $orderBy = null)
 * @method RestAccessToken[]    findAll()
 * @method RestAccessToken[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RestAccessTokenRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, RestAccessToken::class);
    }
}
