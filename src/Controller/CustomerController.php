<?php
namespace App\Controller;

use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\CustomerService;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CustomerController extends AbstractController
{
    private $customerService;
    private $serializerBuilder;
    private $serializationContext;
    private $jsonResponse;

    public function __construct(
        CustomerService $customerService,
        SerializerBuilder $serializerBuilder,
        SerializationContext $serializationContext,
        JsonResponse $jsonResponse
    ) {
        $this->customerService = $customerService;
        $this->serializerBuilder = $serializerBuilder;
        $this->serializationContext = $serializationContext;
        $this->jsonResponse = $jsonResponse;
    }

    /**
     * @SWG\Get(
     *     path="/api/customers",
     *     @SWG\Response(response="200", description="Retorna o resultado do customers em Json"),
     *     @SWG\Response(response="401", description="Não autorizado"),
     *     @SWG\Parameter(
     *          name="busca",
     *          in="path",
     *          type="string",
     *          required=false,
     *          description="Serão retornados os registros que contêm no campo nome o 'texto' informado."
     *     ),
     *     @SWG\Parameter(
     *          name="pagina",
     *          in="path",
     *          type="integer",
     *          required=false,
     *          description="Paginação de registros."
     *     ),
     *     @SWG\Parameter(
     *          name="limite",
     *          in="path",
     *          type="integer",
     *          required=false,
     *          description="Limite de registros mostrado por cada página, padrão é 10."
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @Route("/api/customers", methods={"GET"}, name="getCustomers" )
     */
    public function getCustomers(Request $request): JsonResponse
    {
        $customers = $this->customerService->getCustomers($request);
        $serializer = $this->serializerBuilder->create()->build();
        $customerJson = $serializer->serialize(
            $customers,
            'json',
            $this->serializationContext->create()->setGroups(['basic'])
        );
        return $this->jsonResponse->fromJsonString($customerJson, 200);
    }

    /**
     * @SWG\Get(
     *     path="/api/customers/{customer}",
     *     @SWG\Response(response="200", description="Retorna o resultado do customers em Json"),
     *     @SWG\Response(response="401", description="Não autorizado"),
     *     security={{"Bearer":{}}}
     * )
     * @Route("/api/customers/{customer}", methods={"GET"}, name="getCustomer" )
     */
    public function getCustomer($customer): JsonResponse
    {
        $customers = $this->customerService->getCustomer($customer);
        $serializer = $this->serializerBuilder->create()->build();
        $customerJson = $serializer->serialize(
            $customers,
            'json',
            $this->serializationContext->create()->setGroups(['basic'])
        );
        return $this->jsonResponse->fromJsonString($customerJson, 200);
    }

    /**
     * @SWG\Post(
     *     path="/api/customers",
     *     @SWG\Response(response="201", description="Criado no banco"),
     *     @SWG\Response(response="401", description="Não autorizado"),
     *     @SWG\Parameter(
     *          name="name",
     *          in="path",
     *          type="string",
     *          required=true,
     *          description="O nome do cliente a ser criado."
     *     ),
     *     @SWG\Parameter(
     *          name="cpf",
     *          in="path",
     *          type="string",
     *          required=true,
     *          description="O cpf do cliente a ser criado."
     *     ),
     *     @SWG\Parameter(
     *          name="dataNascimento",
     *          in="path",
     *          type="string",
     *          required=true,
     *          description="A data de nascimento do cliente a ser criado."
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @Route("/api/customers", methods={"POST"}, name="insertCustomer" )
     */
    public function insertCustomer(Request $request): JsonResponse
    {
        $customer = $this->customerService->insertCustomer($request);
        if (is_string($customer)) {
            return $this->jsonResponse->fromJsonString($customer, 500);
        } else {
            $serializer = $this->serializerBuilder->create()->build();
            $customerJson = $serializer->serialize(
                $customer,
                'json',
                $this->serializationContext->create()->setGroups(['basic'])
            );
            return $this->jsonResponse->fromJsonString($customerJson, 201);
        }
    }

    /**
     * @SWG\Put(
     *     path="/api/customers/{customer}",
     *     @SWG\Response(response="201", description="Criado no banco"),
     *     @SWG\Response(response="401", description="Não autorizado"),
     *     @SWG\Parameter(
     *          name="name",
     *          in="path",
     *          type="string",
     *          required=true,
     *          description="O nome do cliente atualizado."
     *     ),
     *     @SWG\Parameter(
     *          name="cpf",
     *          in="path",
     *          type="string",
     *          required=true,
     *          description="O cpf do cliente atualizado."
     *     ),
     *     @SWG\Parameter(
     *          name="dataNascimento",
     *          in="path",
     *          type="string",
     *          required=true,
     *          description="A data de nascimento do cliente atualizado."
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @SWG\Patch(
     *     path="/api/customers/{customer}",
     *     @SWG\Response(response="201", description="Criado no banco"),
     *     @SWG\Response(response="401", description="Não autorizado"),
     *     @SWG\Parameter(
     *          name="name",
     *          in="path",
     *          type="string",
     *          required=true,
     *          description="O nome do cliente atualizado."
     *     ),
     *     @SWG\Parameter(
     *          name="cpf",
     *          in="path",
     *          type="string",
     *          required=true,
     *          description="O cpf do cliente atualizado."
     *     ),
     *     @SWG\Parameter(
     *          name="dataNascimento",
     *          in="path",
     *          type="string",
     *          required=true,
     *          description="A data de nascimento do cliente atualizado."
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * @Route("/api/customers/{customer}", methods={"PUT","PATCH"}, name="updateCustomer" )
     */
    public function updateCustomer(Request $request, $customer): JsonResponse
    {
        $customer = $this->customerService->updateCustomer($request, $customer);
        if (is_string($customer)) {
            return $this->jsonResponse->fromJsonString($customer, 500);
        } else {
            $serializer = $this->serializerBuilder->create()->build();
            $customerJson = $serializer->serialize(
                $customer,
                'json',
                $this->serializationContext->create()->setGroups(['basic'])
            );
            return $this->jsonResponse->fromJsonString($customerJson, 201);
        }
    }

    /**
     * @SWG\Delete(
     *     path="/api/customers/{customer}",
     *     @SWG\Response(response="200", description="Retorna o resultado do customers em JSON"),
     *     @SWG\Response(response="401", description="Não autorizado"),
     *     security={{"Bearer":{}}}
     * )
     * @Route("/api/customers/{customer}", methods={"DELETE"}, name="deleteCustomer" )
     */
    public function deleteCustomer($customer): JsonResponse
    {
        $customer = $this->customerService->deleteCustomer($customer);
        $serializer = $this->serializerBuilder->create()->build();
        $customerJson = $serializer->serialize(
            [],
            'json',
            $this->serializationContext->create()->setGroups(['basic'])
        );
        return $this->jsonResponse->fromJsonString($customerJson, 200);
    }
}