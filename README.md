Projeto API RESTful em Symfony.
==========

API RESTful de cadastro de clientes. 
Onde o cliente possui 3 atributos: Nome, CPF e data de nascimento.

Desenvolvido com framework symfony 4.1.4 com autenticação Oauth2.

## Install

**Este pacote foi desenvolvido para uso do [Composer](https://getcomposer.org/), portanto não será explicitada nenhuma alternativa de instalação.**

*E deve ser instalado com:*
```bash
cd PATH
git clone https://gitlab.com/ericopep/restful-symfony.git
```

*Logo após vc terá que instalar as dependencias do projeto*
```bash
cd PATH
composer install
```
*Iniciar servidor*
```bash
php -S localhost:8000 -t public/
```

## Database

**Abra o arquivo .env, configura por exemplo (DATABASE_URL=mysql://usuario:senha@servidor:porta/banco) e configure a conexão do Banco de Dados**

*Logo após vc terá que criar o banco de dados e as tabelas*
```bash
cd PATH
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate
```

## Adicionar Oauth2 client

**Criar um Client Oauth2**

*O rest_client foi criado pelo migrations, faltando apenas a criação do usuário*

*vamos criar o usuário*
```bash
php bin/console fos:user:create
Please choose a username:admin
Please choose an email:admin@example.com
Please choose a password:admin
Created user admin
```

**Oauth2 token**

*Ambiente de desenvolvimento de API*
```bash
Postman https://www.getpostman.com/apps
```

*Exemplo*
```bash
http POST http://localhost:8000/oauth/v2/token \
    grant_type=password \
    client_id=1_3bcbxd9e24g0gk4swg0kwgcwg4o8k8g4g888kwc44gcc0gwwk4 \
    client_secret=4ok2x70rlfokc8g0wws8c8kwcokw80k44sg48goc0ok4w0so0k \
    username=admin \
    password=admin
HTTP/1.1 200 OK
Cache-Control: no-store, private
Connection: close
Content-Type: application/json
...

{
    "access_token": "MDFjZGI1MTg4MTk3YmEwOWJmMzA4NmRiMTgxNTM0ZDc1MGI3NDgzYjIwNmI3NGQ0NGE0YTQ5YTVhNmNlNDZhZQ",
    "expires_in": 3600,
    "refresh_token": "ZjYyOWY5Yzg3MTg0MDU4NWJhYzIwZWI4MDQzZTg4NWJjYzEyNzAwODUwYmQ4NjlhMDE3OGY4ZDk4N2U5OGU2Ng",
    "scope": null,
    "token_type": "bearer"
}
```

*exemplo*
```bash
http GET http://localhost:8000/api/customers \
    "Authorization:Bearer MDFjZGI1MTg4MTk3YmEwOWJmMzA4NmRiMTgxNTM0ZDc1MGI3NDgzYjIwNmI3NGQ0NGE0YTQ5YTVhNmNlNDZhZQ"
HTTP/1.1 200 OK
Cache-Control: no-cache
Connection: close
Content-Type: application/json
...

{
    Retorno Json
}
```

## Documentação

*Acesso a documentação*
```bash
NelmioApiDoc http://localhost:8000/api/doc
```

## Desenvolvido

Desenvolvido em 01 de dezembro de 2018.

Desenvolvedor: Pedro Érico
Email: pedroerico.desenvolvedor@gmail.com